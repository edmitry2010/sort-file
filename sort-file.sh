#!/bin/bash
# Пример запуска
# ./sort-file.sh /mnt/disk2/samba/scan_buh

namein=$(echo $0 | awk -F'/' '{print $NF}')
perin=($*)

dir_conf='/etc/myserver/scrypts/sort-file'

if ! [ -d $dir_conf ]; then
  dir_conf=`pwd`
  perin=init
fi

if ! [ -f $dir_conf/$namein ]; then
  dir_conf=`pwd`
  perin=init
fi

# Массив с бинарниками которые потребуются в скрипте для корректной работы в crontab
ArrSearchBin=( sleep mkdir xargs date find grep perl echo sed mv )

##### BIN beginning #####
bin_sleep='/usr/bin/sleep'
bin_mkdir='/usr/bin/mkdir'
bin_xargs='/usr/bin/xargs'
bin_date='/usr/bin/date'
bin_find='/usr/bin/find'
bin_grep='/usr/bin/grep'
bin_perl='/usr/bin/perl'
bin_echo='/usr/bin/echo'
bin_sed='/usr/bin/sed'
bin_mv='/usr/bin/mv'

. $dir_conf/lib/color.table
. $dir_conf/lib/sort.func

if [[ $perin == init ]]; then
  PrintDisplay "$ViewYellow Предварительная настройка"
  StartInit
fi

TestBin $bin_date $bin_mkdir $bin_xargs $bin_find

# Получаем папочку для сортировки
dir_sort=$($bin_echo ${perin[@]} | $bin_sed 's|/$||g')

# echo 'Тут берётся текущее время в секундах, минус количество дней в секундах'
date_tek_del=$(( `$bin_date +%s` - 604800 )) # Текущее время, минус неделя в секундах
#date_tek_del=$(( `$bin_date +%s` - 172800 )) # Текущее время, минус 2 дня в секундах

# Минимум дней для поиска
min_day='1'

# 1. Проверяем, что вы передали в качестве параметра попочку для сортировки.
# 2. Проверяет, что эта папочка существует.
# 3. Смотрит файлы и папки не глубже чем сама указанная папочка.
# 4. Исключает из поиска саму папочку для поиска, для передачи на дальнейшую обработку.
# 5. Исключает из поиска папочки с годом, начиная с 2000 до 3000 года, хватит до конца тысячилетия).
#    Это требуется для того, что бы скрипт не переместил уже сортированные папочки.
#    Папки с годом 2ХХХ, будут создаваться скриптом, не следует создавать похожие папки, они попадут в исключение.
if ! [ -z $perin ]; then
  if [ -d $dir_sort ]; then
    while read FileLine; do
      if ! [ -z "$FileLine" ]; then
	PreSort "$FileLine"
      fi
      unset FileLine
    # В случае если точно знаете, что файлы и папочки моложе указанных дней в min_day не требуется сортировать.
    # Можете раскомментировать нижнюю строку, закомментировать строку под ней и указать в переменной min_day количество дней,
    # файлы моложе которых скрит не будет передавать на сортировку.
#    done < <($bin_find $dir_sort -maxdepth 1 -mtime +$min_day | $bin_sed "s|$dir_sort/||g" | $bin_grep -wvE "(^2[0-9]{3})")
    done < <($bin_find $dir_sort -maxdepth 1 | $bin_sed "s|$dir_sort/||g" | $bin_grep -wvE "(^2[0-9]{3})")
  fi
fi
